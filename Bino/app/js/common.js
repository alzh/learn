$(function () {
    $('.js-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1
        // fade: true,
    });
    
    $('.js-slider-vertical').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        // vertical: true,
        // verticalSwiping: true,
        dots: true,
        arrows: false,
        initialSlide: 1
    });

    // Header fade in/out
    let $header = $('.js-header');
    let $scrolledPrev = $(window).scrollTop();
    let $nav = $('.js-nav');
    $(window).on('scroll', function(event) {
        $nav.removeClass('show');
        $('.burger').removeClass('is-open');
        const $scrolled = $(this).scrollTop();
    
        if($scrolled >= $scrolledPrev) {
            // вниз
            if($header.hasClass('fixed')) {
                $header.addClass('hidden');
    
                setTimeout(function() {
                    $header.removeClass('fixed');
                }, 300);
            }
        } else {
            // вверх
            if(!$header.hasClass('fixed')) {
                $header.addClass('hidden noanim fixed');
                
                setTimeout(function() {
                    $header.removeClass('noanim');
                }, 0);
                
                setTimeout(function() {
                    $header.removeClass('hidden');
                }, 1);
            }
    
            if($scrolled <= 0) {
                $header.removeClass('fixed');
            }
        }
    
        $scrolledPrev = $scrolled;
    });

    // Burger Menu
    let $burger = $(".js-burger");
    let $navLink = $('.js-nav__link')

    $burger.on("click", function(event) {
        event.preventDefault();

        setTimeout(function() {
            $nav.slideToggle();
        }, 300);
        
        $burger.toggleClass('is-open');
    });

    $navLink.on("click", function(event) {
        event.preventDefault();
        setTimeout(function() {
            $nav.slideUp();
        }, 300);
        $burger.removeClass('is-open');
    });

    $(document).on('click', function (event) {
        if(!$(event.target).closest('.header').length) {
            if($burger.hasClass('is-open')) {
                $nav.slideUp();
                $burger.removeClass('is-open');
            } 
        }
    });

    // Скролл по нажатию на кнопки меню
    $("[data-scroll]").on("click", function(event) {
        event.preventDefault();

        var elementID = $(this).data('scroll');
        var elementOffset = $(elementID).offset().top;
        var headerH = $(".js-header").innerHeight(); 

        // $('.js-nav a').removeClass('active');
        // $(this).addClass('active');

        $("html, body").animate({
            scrollTop: elementOffset
        }, 700);
    });    

        $(".js-logo").on("click", function(event) {
            $("html, body").animate({
                scrollTop: 0
            }, 700);
    });

    // Скролл по нажатию на якорь в Intro
    $('.js-anchor').on("click", function(event) {
        event.preventDefault();

        var introH = $(".js-intro").innerHeight(); 
        
        $("html, body").animate({
            scrollTop: introH + 60
        }, 700);


    });

    // Функция для анимированного показа блоков
    function animateScroll() {
        $('[data-animate]').each(function () {
        // Множитель высоты экрана, чтобы задать, на какой точке должна запускаться анимация. По умолчанию - 80% от высоты экрана
            var delta = 0.6;
    
        // Если у элемента есть дополнительный атрибут "data-animate-nooffset", то убираем множить (единица не влияет на результат при умножении). Пример использования - футер
            if ($(this).is('[data-animate-nooffset]')) {
            delta = 1;
            }
    
        var scrolled = $(document).scrollTop() + window.innerHeight * delta;
        var offset = $(this).offset().top;
            if (offset <= scrolled) {
                var cl = $(this).attr('data-animate');
                $(this).addClass(cl).removeAttr('data-animate');
            }
        });
    }
  
    // Вызываем функцию при загрузке страницы
    animateScroll();
    // Вызываем функцию при скролле страницы
    $(document).on('scroll', animateScroll);

    // Form Validation        
    $('[data-validate]').validate({
        errorPlacement: function(error, element) {
            var $parent = element.parent();
            $parent.append(error);
        },
        submitHandler: function(form) {
                $(form).trigger("formSubmit");
        }
    });
    
    $('.js-input-name').on('input', function() {
        var value = $(this).val();
        value = value.replace(/[^a-zA-Zа-яА-ЯЁё\s\-]/ig, '');
        $(this).val(value);
    });

   
    let $input = $('.js-input');
    let $label = $('.js-label');

    $input.on('blur', function(event) {
        var inputValue = event.target.value;
        if(inputValue.length) {
            $(this).siblings('.js-label').addClass('contacts__label--focused');
        } else {
            $(this).siblings('.js-label').removeClass('contacts__label--focused');
        }
    })

    $input.on('focus', function(event) {
        $(this).siblings('.js-label').addClass('contacts__label--focused');
    })


})




