// Common
var gulp          = require('gulp');
var rename        = require('gulp-rename');
var del           = require('del');
var browserSync   = require('browser-sync');
var sourcemaps    = require('gulp-sourcemaps');

// HTML
var pug           = require('gulp-pug');

// CSS
var sass          = require('gulp-sass');
var cssnano       = require('gulp-cssnano');
var autoprefixer  = require('gulp-autoprefixer');

// JS
var uglify        = require('gulp-uglify-es').default;
var fileinclude   = require('gulp-file-include');

// gulp.task('task1', function() {
//     return gulp.src('app/**/*.*')
//             .pipe(gulp.dest('dist'));
// }); 

gulp.task('html', function() {
    return gulp.src(['app/**/*.pug', '!app/**/_*.pug'])
            .pipe(pug({
                pretty: true
            }))
            .pipe(sourcemaps.write('../maps'))
            .pipe(gulp.dest('dist/'))
            .pipe(browserSync.reload({ stream: true }))
});

gulp.task('css', function() {
    return gulp.src(['app/scss/**/*.scss', '!app/scss/**/_*.scss', '!app/scss/libs.scss'])
            .pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(autoprefixer(['last 15 versions', '>1%'], { cascade: true }))
            // .pipe(cssnano())
            .pipe(rename({ suffix: '.min' }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('dist/css'))
            .pipe(browserSync.stream())
});

gulp.task('css-build', function() {
    return gulp.src(['app/scss/**/*.scss', '!app/scss/**/_*.scss', '!app/scss/libs.scss'])
            .pipe(sass())
            .pipe(autoprefixer(['last 15 versions', '>1%'], { cascade: true }))
            .pipe(cssnano())
            .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest('dist/css'))
            .pipe(browserSync.stream())
});

gulp.task('css-libs', function() {
    return gulp.src('app/scss/libs.scss')
            .pipe(sourcemaps.init())
            .pipe(sass())
            .pipe(autoprefixer(['last 15 versions', '>1%'], { cascade: true }))
            .pipe(cssnano())
            .pipe(rename({ suffix: '.min' }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('dist/css'))
            .pipe(browserSync.stream())
});

gulp.task('css-libs-build', function() {
    return gulp.src('app/scss/libs.scss')
            .pipe(sass())
            .pipe(autoprefixer(['last 15 versions', '>1%'], { cascade: true }))
            .pipe(cssnano())
            .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest('dist/css'))
            .pipe(browserSync.stream())
});

gulp.task('js', function() {
    return gulp.src(['app/js/**/*.js', '!app/js/**/_*.js', '!app/js/libs.js'])
            .pipe(sourcemaps.init())
            // .pipe(uglify())
            .pipe(rename({ suffix: '.min' }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('dist/js'))
            .pipe(browserSync.reload({ stream: true }))
});

gulp.task('js-build', function() {
    return gulp.src(['app/js/**/*.js', '!app/js/**/_*.js', '!app/js/libs.js'])
            .pipe(uglify())
            .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest('dist/js'))
            .pipe(browserSync.reload({ stream: true }))
});

gulp.task('js-libs', function() {
    return gulp.src(['app/js/libs.js'])
            .pipe(sourcemaps.init())
            .pipe(fileinclude({
                prefix: '@@',
                basepath: '@file',
                indent: 'true'
            }))
            .pipe(uglify())
            .pipe(rename({ suffix: '.min' }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest('dist/js'))
            .pipe(browserSync.reload({ stream: true }))
});

gulp.task('js-libs-build', function() {
    return gulp.src(['app/js/libs.js'])
            .pipe(fileinclude({
                prefix: '@@',
                basepath: '@file',
                indent: 'true'
            }))
            .pipe(uglify())
            .pipe(rename({ suffix: '.min' }))
            .pipe(gulp.dest('dist/js'))
            .pipe(browserSync.reload({ stream: true }))
});

gulp.task('assets', function() {
    return gulp.src(['app/assets/**/*.*'])
            .pipe(gulp.dest('dist/assets'))
            .pipe(browserSync.reload({ stream: true }))
});

gulp.task('clean', function() {
    return del('dist/*')
});

gulp.task('webserver', function() {
    browserSync({
        server: {
            baseDir: 'dist'
        }
    })
})

gulp.task('watch', function() {
    gulp.watch(['app/**/*.pug'], gulp.series('html'));
    gulp.watch(['app/scss/**/*.scss', '!app/scss/libs.scss'], gulp.series('css'));
    gulp.watch(['app/scss/libs.scss'], gulp.series('css-libs'));
    gulp.watch(['app/js/**/*.js', '!app/js/libs.js'], gulp.series('js'));
    gulp.watch(['app/js/libs.js'], gulp.series('js-libs'));
})

gulp.task('create', gulp.parallel('assets', 'html', 'css', 'css-libs', 'js', 'js-libs'));
gulp.task('create-build', gulp.series('assets', 'html', 'css-build', 'css-libs-build', 'js-build', 'js-libs-build'));
// 152 series или parallel


gulp.task('build', gulp.series('clean', 'create-build'));
gulp.task('default', gulp.series('clean', 'create', gulp.parallel('webserver', 'watch')));