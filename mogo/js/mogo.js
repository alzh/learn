
$(function() {
    /* Fixed header */ 

    let header = $(".js-header");
    let scrolledPrev = $(window).scrollTop();
    $(window).on('scroll', function(event) {
        nav.removeClass('show');
        $('.burger').removeClass('is-open');
        const scrolled = $(this).scrollTop();
    
        if(scrolled >= scrolledPrev) {
            // вниз
            if(header.hasClass('fixed')) {
                header.addClass('hidden');
    
                setTimeout(function() {
                    header.removeClass('fixed');
                }, 300);
            }
        } else {
            // вверх
            if(!header.hasClass('fixed')) {
                header.addClass('hidden noanim fixed');
                
                setTimeout(function() {
                    header.removeClass('noanim');
                }, 0);
                
                setTimeout(function() {
                    header.removeClass('hidden');
                }, 1);
            }
    
            if(scrolled <= 0) {
                header.removeClass('fixed');
            }
        }
    
        scrolledPrev = scrolled;
    });

    /*Smooth Scroll*/  
    $("[data-scroll]").on("click", function(event) {
        event.preventDefault();

        var elementID = $(this).data('scroll');
        var elementOffset = $(elementID).offset().top;
        var headerH = $(".js-header").innerHeight(); 

        $('.js-nav a').removeClass('active');
        $(this).addClass('active');

        $("html, body").animate({
            scrollTop: elementOffset
        }, 700);
    });    

        $(".js-logo").on("click", function(event) {
            $("html, body").animate({
                scrollTop: 0
            }, 700);
    });

    /* Burger menu */
    let burger = $('.js-burger'),
    nav = $('.js-nav');

    burger.on("click", function(event) {
        event.preventDefault();

        setTimeout(function() {
            nav.slideToggle();
        // nav.toggleClass("js-nav-show");
        }, 300);
        
        $('.burger').toggleClass('is-open');
    });

    $(document).on('click', function (event) {
        if(!$(event.target).closest('.header').length) {
            nav.slideUp();
            $('.burger').removeClass('is-open');
        }
    });

    /* Accordion */
    $('.js-accordion__item').on('click', function() {
        event.preventDefault();

        var $this = $(this),
        blockId = $this.data('collapse');

        $this.toggleClass('active');
    });

    /* Slider */
    $("[data-slider]").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        // fade: true,

    });


});